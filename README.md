# Deployment

```
composer install --no-dev --optimize-autoloader
composer dump-env prod
php bin/console cache:clear
php bin/console asset-map:compile
php bin/console cache:warmup
```
