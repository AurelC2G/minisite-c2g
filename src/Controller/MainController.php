<?php

namespace App\Controller;

use App\GameDate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class MainController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(): Response
    {
        return $this->render('index.html.twig');
    }

    #[Route('/aide', name: 'aide')]
    public function aide(): Response
    {
        return $this->render('aide.html.twig');
    }

    #[Route('/annexe', name: 'annexe')]
    public function annexe(): Response
    {
        return $this->render('annexe.html.twig');
    }

    #[Route('/debuter', name: 'debuter')]
    public function debuter(): Response
    {
        return $this->render('debuter.html.twig');
    }

    #[Route('/rp', name: 'rp')]
    public function rp(): Response
    {
        return $this->render('definition_rp.html.twig');
    }

    #[Route('/historique/{page}', name: 'historique', requirements: ['page' => '1|2|3'])]
    public function historique(int $page = 1): Response
    {
        return $this->render('historique_'.$page.'.html.twig');
    }

    #[Route('/univers', name: 'univers')]
    public function univers(Request $request): Response
    {
        $mode = $request->request->get('mode');
        $conversionResult = '';
        if ($mode == 'irl2c2g') {
            $date = explode('/', $request->request->get('date'));
            if (count($date) == 3 and $date[2] >= 1970) {
                $t = mktime(0, 0, 0, $date[1], $date[0], $date[2]);

                //Correction pour éviter un bug lié à l'heure d'hiver : on ajoute 3 heures
                $t += 3 * 3600;

                $conversionResult = $request->request->get('date').' >> '.GameDate::irl2c2g($t);
            }
        } elseif ($mode == 'c2g2irl') {
            $d = $request->request->get('day');
            $s = $request->request->get('season');
            $y = $request->request->get('year');

            $date = GameDate::c2g2irl($d, $s, $y);
            if ($date) {
                $conversionResult = $d.' '.$s.' '.$y.' >> '.date("d/m/Y", $date);
            }
        }

        return $this->render('univers.html.twig', [
            'currentDate' => GameDate::irl2c2g(),
            'conversionResult' => $conversionResult,
        ]);
    }
}
