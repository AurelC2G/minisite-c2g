<?php

namespace App;

class GameDate
{
    public static function irl2c2g($time = -1, $raccourci = false): string
    {
        if ($time < 0) {
            $time = time();
        }

        //On ajoute 119531462400 secondes pour être en phase avec le calendrier des Chroniques
        $time += 119531462400;

        $date = floor($time / 86400);

        $an = floor($date / 375);
        $date -= 375 * $an;

        $saison = floor($date / 75);
        $date -= 75 * $saison;

        $jour = 1 + $date;

        if ($raccourci) {
            $liste_saisons = array("Ga","Aq","Ve","Vo","De");
            if ($jour < 10) {
                $jour = "0".$jour;
            }
        } else {
            $liste_saisons = array("Galan","Aquan","Vertan","Volcan","Desertan");
            if ($jour == 1) {
                $jour = "1<sup>er</sup>";
            }
        }

        return $jour." ".$liste_saisons[$saison]." ".$an;
    }

    public static function c2g2irl($day, $season, $year): ?int
    {
        $day = (int)$day;
        if ($day < 1 or $day > 75) {
            return null;
        }

        $liste_saisons = array("Galan","Aquan","Vertan","Volcan","Desertan");
        $season = array_search($season, $liste_saisons);
        if ($season === false) {
            return null;
        }

        $year = (int)$year;

        //Quand le timestamp unix vallait, on était le 17 Aquan 3689, + 0 secondes.
        $day -= 17;
        $season -= 1;
        $year -= 3689;

        //On reconstitue le timestamp
        $time = ($day + 75 * ($season + 5 * $year)) * 86400;

        if ($time < 0) {
            return null;
        } else {
            return $time;
        }
    }
}
